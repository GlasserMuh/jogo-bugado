﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformGernation : MonoBehaviour {

    // Use this for initialization
    public GameObject plataformComOb;
    public GameObject plataform;
    private GameObject clone;

    public Transform generationpoint;
    public float distancebtweenplat;

    private float plataformwidth;

	void Start () {

        plataformwidth = plataform.GetComponent<BoxCollider2D>().size.x;
	}
	
	// Update is called once per frame
	void Update () {

        if(transform.position.x<generationpoint.position.x)
        {
            SpawnaGround();
        }
		
	}


    void SpawnaGround()
    {

        int rand = Random.RandomRange(1, 3);
        int escolheplat = Random.RandomRange(1, 3);

        if (rand == 2)
        {
            distancebtweenplat = 0;
        }

        transform.position = new Vector3(transform.position.x + plataformwidth + distancebtweenplat, transform.position.y, transform.position.z);

        if (escolheplat == 1)
        {
            Instantiate(plataform, transform.position, transform.rotation);
        }
        if (escolheplat == 2)
        {
            Instantiate(plataformComOb, transform.position, transform.rotation);
        }
        distancebtweenplat = 10;


    }
}
