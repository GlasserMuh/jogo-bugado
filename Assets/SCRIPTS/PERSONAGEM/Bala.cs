﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

    public float speed;

	// Use this for initialization
    public Rigidbody2D BalaRB;
	void Start () {
		if(GameManager.Instance.dir==true)
        {
            BalaRB.velocity = new Vector2(speed, BalaRB.velocity.y);

        }
        if (GameManager.Instance.esq == true)
        {
            BalaRB.velocity = new Vector2(-speed, BalaRB.velocity.y);

        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
