﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PERSONAGEM : MonoBehaviour
{



    //Variaveis inteiras
    public static int Vida = 0;
    public int puloduplo;



    //Variaveis Booleanas
    public bool colisao;
    public bool colisao2;
    public bool colisao3;
    public bool cim;
    public static bool RightEye = false;
    public bool noar = false;
    bool shooting=false;
    bool porreting=false;

    //Variaveis float
    public float raio;
    public float fpulo;
    public float Playerspeed;
    public float Playerspeed2;
    public float bouncenemy;
    public float hurtforce=10;
    float tempo = 0;
    float contatt = 0;
    float tempoporrete=0;
    float contattporrete = 0;

    //Variaveis unity
    public LayerMask layerChao;
    public LayerMask plat;
    public LayerMask dinos;
    public Animator animador;
    public Transform spawnbala;
    public GameObject detectarChao;
    public GameObject bala;
    public Rigidbody2D RB;
    public GameObject dino;


    GameObject clonebala;


    // Update is called once per frame
    void Update()
    {

        if (tempo >= 0.1f)
        {
            tempo = 0;
            contatt = 0;
        }



        if (tempoporrete >= 0.6f)
        {
            tempoporrete = 0;
            contattporrete = 0;
        }
        tempo = tempo + contatt * Time.deltaTime;
        tempoporrete = tempoporrete + contattporrete * Time.deltaTime;

        if (Vida < 30)
        {
            if (Input.GetKey(KeyCode.D))
            {
                RB.velocity = new Vector2(Playerspeed, RB.velocity.y);
                animador.SetBool("ANDANDO", true);
                GameManager.Instance.dir = true;
                GameManager.Instance.esq = false;
                Flip();
            }

            else if (Input.GetKey(KeyCode.A))
            {
                RB.velocity = new Vector2(Playerspeed2, RB.velocity.y);
                animador.SetBool("ANDANDO", true);
                GameManager.Instance.dir = false;
                GameManager.Instance.esq = true;
                Flip();
            }
            else
            {
                RB.velocity = new Vector2(0, RB.velocity.y);
                animador.SetBool("ANDANDO", false);
            }
            colisao = Physics2D.OverlapCircle(detectarChao.transform.position, raio, layerChao);
            colisao2 = Physics2D.OverlapCircle(detectarChao.transform.position, raio, plat);
            colisao3 = Physics2D.OverlapCircle(detectarChao.transform.position, raio, dinos);

            if (Input.GetKeyDown(KeyCode.Space) && (colisao == true || colisao2 == true || colisao3 == true))
            {

                jump();

            }

            if (Input.GetKeyDown(KeyCode.L))
            {


                Atirar();


            }
            else if (Input.GetKeyDown(KeyCode.K) && (colisao == true || colisao2 == true || colisao3 == true))
            {

                Bater();


            }


            animador.SetBool("pulo", colisao || colisao2 || colisao3);

        }
        else
        {

            RB.velocity = new Vector2(0, RB.velocity.y);
            animador.SetBool("MORREU", true);
        }

        if (!colisao && puloduplo < 1 && noar == true)
        {


            if (Input.GetKeyDown(KeyCode.Space))
            {
                animador.SetBool("DJUMP", true);
                RB.AddForce(transform.up * 2500);
                puloduplo++;
            }





        }


        if (colisao || colisao2 || colisao3)
        {
            puloduplo = 0;
            noar = false;
            animador.SetBool("DJUMP", false);
        }
        else { noar = true; }

        if (Vida >= 30)
        {

            noar = false;
            animador.SetBool("DJUMP", false);
        }

    }
    void jump()
    {

        RB.AddForce(transform.up * fpulo);



    }


    void Flip()
    {
        if (Input.GetAxisRaw("Horizontal") < 0 && RightEye == false)
        {

            RB.transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            RightEye = true;
        }

        if (Input.GetAxisRaw("Horizontal") > 0 && RightEye == true)
        {

            RB.transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
            RightEye = false;
        }
    }

    void Atirar()
    {
        if (tempo == 0)
        {
            clonebala = Instantiate(bala, spawnbala.position, Quaternion.identity);
            contatt = 1;
 
        }


    }


    void Bater()
    {
        if (tempoporrete == 0)
        {
            animador.SetBool("ATKPORRETE", true);
            contattporrete = 1;
            Playerspeed = 1;
            Playerspeed2 = -1;
  

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Boca_Dino")
        {
            Debug.Log("BOCA");
            if (JOELHODINO.caindo != true && Vida < 10)
            {
                if (noar==false)
                {

                    TakeDamage(collision.transform);
                }
            }





        }

    }

    public void setabool()
    {


        animador.SetBool("ATKPORRETE", false);
        Playerspeed = 10;
        Playerspeed2 = -10;



    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Dino")
        {
            Debug.Log("DINO");
            if (JOELHODINO.caindo != true && Vida < 30)
            {
                if (noar == false)
                {
                    TakeDamage(collision.transform);
                }
                Vida++;
            }






        }




    }

    void TakeDamage(Transform enemy)
    {
     

        // Create a vector that's from the enemy to the player with an upwards boost.
        Vector3 hurtVector = transform.position - enemy.position + Vector3.up * 7f;

        RB.AddForce(hurtVector * 300);


    }
}