﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DINO : MonoBehaviour {


    //Variaveis inteiras
    public static int Dino_Vida = 66;

    //Variaveis decimais
    public static float DinoSpeed = 0;
    public static float distanciadinoperso = 8;

    //Variaveis booleanas
    bool atkbsc = false;
    bool RightEye = true;
    bool atquebasicoo = false;

    //Variaveis Unity
    public GameObject personagem;
    private Rigidbody2D rb;
    private Transform mytransform;
    public GameObject inimigo;
    public LayerMask perso;
    public GameObject atacarperso;
    public Transform pontoATK1;
    public Transform pontoATK2;
    public Transform pontocabe1;
    public Transform pontocabe2;
    public Animator animador;

    public bool persoemcima=false;
    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>();
        mytransform = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update() {

       if (PERSONAGEM.RightEye == true)
        {
            if ((inimigo.transform.position.y > pontocabe1.transform.position.y &&
                inimigo.transform.position.y > pontocabe2.transform.position.y) &&
                (inimigo.transform.position.x < pontocabe1.transform.position.x &&
                inimigo.transform.position.x > pontocabe2.transform.position.x))
            {


                persoemcima = true;
                animador.SetBool("PARADO",true);
            }
            else {
                persoemcima = false;
                animador.SetBool("PARADO", false);
            }

        }
        else
        {

            if ((inimigo.transform.position.y > pontocabe1.transform.position.y &&
              inimigo.transform.position.y > pontocabe2.transform.position.y) &&
              (inimigo.transform.position.x > pontocabe1.transform.position.x &&
              inimigo.transform.position.x < pontocabe2.transform.position.x))
            {


                persoemcima = true;
                animador.SetBool("PARADO", true);
            }
            else
            {
                persoemcima = false;
                animador.SetBool("PARADO", false);
            }


        }
    
        atquebasicoo = Physics2D.OverlapCircle(atacarperso.transform.position, 5, perso);
        if (PERSONAGEM.Vida<30)
        {
            if (JOELHODINO.caindo == false)
            {
                if (atquebasicoo == true && (inimigo.transform.position.y < pontoATK1.position.y && inimigo.transform.position.y > pontoATK2.position.y))
                {


                    animador.SetBool("ATAQUESIMPLES", true);
                    DinoSpeed = 5;
                    atkbsc = true;
                } else
                {
                    if (animador.GetBool("LEVANTANDO") != true &&persoemcima==false)
                    {
                        DinoSpeed = 8;
                    }
                    else
                    {

                
                        DinoSpeed = 0;
                    }
                    animador.SetBool("ATAQUESIMPLES", false);
                    atkbsc = false;
                }
            }
            Flip();

            mytransform.position = Vector2.MoveTowards(transform.position, inimigo.transform.position, DinoSpeed * Time.deltaTime);
        }else
        {

            animador.SetBool("PARADO",true);

        }


    }



    void Flip()
    {
        if (JOELHODINO.caindo == false)
        {
            if (inimigo.transform.position.x > personagem.transform.position.x && RightEye == false)
            {

                rb.transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

                RightEye = true;
            }

            if (inimigo.transform.position.x < personagem.transform.position.x && RightEye == true)
            {

                rb.transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                RightEye = false;
            }
        }
    }



    public void SETABOOLS()
    {

        animador.SetBool("LEVANTANDO", false);
        DINO.DinoSpeed = 8;
        JOELHODINO.DerrubaDinoCont = 0;
    }


 


}
