﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OFSET : MonoBehaviour {

    public float speed;
    private float ofset;
    public Material currentmaterial;
    
  
	
	// Update is called once per frame
	void Update () {
        ofset += 0.001f;
        currentmaterial.SetTextureOffset("_MainTex", new Vector2(ofset * speed, 0));

    }


}
