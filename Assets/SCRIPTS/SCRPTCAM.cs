﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCRPTCAM : MonoBehaviour
{
    private Vector2 velocity;
    private Transform trans;
    public GameObject perso;

    public float SmoothTimeY;
    public float SmoothTimeX;

    // Use this for initialization
    void Start()
    {

        trans = GetComponent<Transform>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        
   
    
        float posX = Mathf.SmoothDamp(transform.position.x,perso.transform.position.x,ref velocity.x, SmoothTimeX);
        float posY = Mathf.SmoothDamp(transform.position.y, perso.transform.position.y, ref velocity.y, SmoothTimeY);
        transform.position = new Vector3(posX,posY+1.1f,transform.position.z);
       // transform.position = new Vector3(perso.transform.position.x,perso.transform.position.y+6,transform.position.z);
    }
}
